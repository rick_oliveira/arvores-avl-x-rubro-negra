public class RedBlackTree<T extends Comparable<T>> {
	 
    public static final int BLACK = 0;
    public static final int RED = 1;
    
    public int searchCount;
    public int nodeVisitedCount;
    public int simpleRotationCount;
    public int doubleRotationCount;
    
    public void setSearchCount(int searchCount) {
		this.searchCount = searchCount;
	}
    
    public void setNodeVisitedCount(int nodeVisitedCount) {
		this.nodeVisitedCount = nodeVisitedCount;
	}
    
    public void setDoubleRotationCount(int doubleRotationCount) {
		this.doubleRotationCount = doubleRotationCount;
	}
    
    public void setSimpleRotationCount(int simpleRotationCount) {
		this.simpleRotationCount = simpleRotationCount;
	}
    
    
    public int getDoubleRotationCount() {
		return doubleRotationCount;
	}
    
    public int getNodeVisitedCount() {
		return nodeVisitedCount;
	}
    
    public int getSimpleRotationCount() {
		return simpleRotationCount;
	}
    
    public int getSearchCount() {
		return searchCount;
	}
    
    public class RedBlackNode<T extends Comparable<T>> {
 
        public T key;
        public RedBlackNode<T> left, right, parent;
        public int color;
        
        public RedBlackNode() {
            left = null;
            right = null;
            parent = null;
            color = BLACK;
        }
 
        public RedBlackNode(T key) {
            this();
            this.key = key;
        }
 
    }
 
    private RedBlackNode<T> nil = new RedBlackNode<>();
    private RedBlackNode<T> root = nil;
 
    private void leftRotate(RedBlackNode<T> x) {
    	simpleRotationCount++;
        RedBlackNode<T> y = x.right;
        x.right = y.left;
        if (y.left != nil) {
            y.left.parent = x;
        }
        y.parent = x.parent;
        if (x.parent == nil) {
            root = y;
        } else {
            if (x == x.parent.left) {
                x.parent.left = y;
            } else {
                x.parent.right = y;
            }
        }
        y.left = x;
        x.parent = y;
    }
 
    private void rightRotate(RedBlackNode<T> x) {
    	simpleRotationCount++;
        RedBlackNode<T> y = x.left;
        x.left = y.right;
        if (y.right != nil) {
            y.right.parent = x;
        }
        y.parent = x.parent;
        if (x.parent == nil) {
            root = y;
        } else {
            if (x == x.parent.right) {
                x.parent.right = y;
            } else {
                x.parent.left = y;
            }
        }
        y.right = x;
        x.parent = y;
    }
 
    private void insert(RedBlackNode<T> z) {
 
        RedBlackNode<T> y = nil;
        RedBlackNode<T> x = root;
 
        while (x != nil) {
        	nodeVisitedCount++;
            y = x;
            if (z.key.compareTo(x.key) < 0) {
            	x = x.left;
            } else {
            	x = x.right;
            }
        }
        z.parent = y;
        if (y == nil) {        	
            root = z;
        } else if (z.key.compareTo(y.key) < 0) {
        	
            y.left = z;
        } else {
            y.right = z;
        }
 
        z.left = nil;
        z.right = nil;
        z.color = RED;
        insertFixUp(z);
    }
 
    private void insertFixUp(RedBlackNode<T> z) {
        while (z != root && z.parent.color == RED) {
        	nodeVisitedCount++;
            if (z.parent == z.parent.parent.left) {
            	nodeVisitedCount++;
                RedBlackNode<T> y = z.parent.parent.right;
                if (y.color == RED) {
                    z.parent.color = BLACK;
                    y.color = BLACK;
                    z.parent.parent.color = RED;
                    z = z.parent.parent;
                } else {
                    if (z == z.parent.right) {
                        z = z.parent;
                        leftRotate(z);
                        doubleRotationCount++;
                    }
                    
                    z.parent.color = BLACK;
                    z.parent.parent.color = RED;
                    rightRotate(z.parent.parent);
                }
            } else {
                if (z.parent == z.parent.parent.right) {
                    RedBlackNode<T> y = z.parent.parent.left;
                    if (y.color == RED) {
                        z.parent.color = BLACK;
                        y.color = BLACK;
                        z.parent.parent.color = RED;
                        z = z.parent.parent;
                    } else {
                        if (z == z.parent.left) {
                            z = z.parent;
                            doubleRotationCount++;
                            rightRotate(z);                            
                        }
                        z.parent.color = BLACK;
                        z.parent.parent.color = RED;
                        leftRotate(z.parent.parent);
                    }
                }
            }
        }
        root.color = BLACK;
    }
 
    public void insert(T key) {
        RedBlackNode<T> novo = new RedBlackNode<T>(key);
        insert(novo);
    }
 
    private RedBlackNode<T> remove(RedBlackNode<T> z) {
        RedBlackNode<T> y;
        RedBlackNode<T> x;
        if (z.left == nil || z.right == nil) {
            y = z;
        } else {
            y = sucessor(z);
        }
 
        if (y.left != nil) {
            x = y.left;
        } else {
            x = y.right;
        }
        x.parent = y.parent;
        if (y.parent == nil) {
            root = x;
        } else {
            if (y == y.parent.left) {
                y.parent.left = x;
            } else {
                y.parent.right = x;
            }
        }
        if (y != z) {
            z.key = y.key;
        }
        if (y.color == BLACK) {
            deleteFixUp(x);
        }
        return y;
    }
 
    private void deleteFixUp(RedBlackNode<T> x) {
        RedBlackNode<T> w;
        while (x != root && x.color == BLACK) {
        	nodeVisitedCount++;
            if (x == x.parent.left) {
                w = x.parent.right;
                if (w.color == RED) {
                    w.color = BLACK;
                    x.parent.color = RED;
                    leftRotate(x.parent);
                    w = x.parent.right;
                }
                if (w.left.color == BLACK && w.right.color == BLACK) {
                    w.color = RED;
                    x = x.parent;
                } else {
                    if (w.right.color == BLACK) {
                        w.left.color = BLACK;
                        w.color = RED;
                        rightRotate(w);
                        doubleRotationCount++;
                        w = x.parent.right;
                    }
                    w.color = x.parent.color;
                    x.parent.color = BLACK;
                    w.right.color = BLACK;
                    leftRotate(x.parent);
                    x = root;
                }
            } else {
                w = x.parent.left;
                if (w.color == RED) {
                    w.color = BLACK;
                    x.parent.color = RED;
                    rightRotate(x.parent);
                    w = x.parent.left;
                }
                if (w.right.color == BLACK && w.left.color == BLACK) {
                    w.color = RED;
                    x = x.parent;
                } else {
                    if (w.left.color == BLACK) {
                        w.right.color = BLACK;
                        w.color = RED;
                        leftRotate(w);
                        doubleRotationCount++;
                        w = x.parent.left;
                    }
                    w.color = x.parent.color;
                    x.parent.color = BLACK;
                    w.left.color = BLACK;
                    rightRotate(x.parent);
                    x = root;
                }
            }
        }
        x.color = BLACK;
    }
 
    public RedBlackNode<T> search(T key) {
 
        RedBlackNode<T> aux = root;
 
        while (aux != nil) {
        	nodeVisitedCount++;
            if (key.compareTo(aux.key) < 0) {
                aux = aux.left;
            } else if (key.compareTo(aux.key) > 0) {
                aux = aux.right;
            } else if (key.compareTo(aux.key) == 0) {
                return aux;
            }
        }
 
        return null;
 
    }
 
    private RedBlackNode<T> sucessor(RedBlackNode<T> z) {
    	nodeVisitedCount++;
        RedBlackNode<T> aux = z.right;
        while (aux.left != nil) {
        	nodeVisitedCount++;
            aux = aux.left;
        }
        return aux;
    }
 
    public void remove(T key) {
        RedBlackNode<T> no = search(key);
        remove(no);
    }
 
    public void inOrder(RedBlackNode<T> root) {
 
        RedBlackNode<T> aux = root;
        if (aux == nil) {
            return;
        }
        inOrder(root.left);
        System.out.println(aux.key);
        inOrder(root.right);
    }
 
    public void imprime() {
        inOrder(root);
    }
    
    public RedBlackNode<T> getRoot() {
		return root;
	}
}