import java.util.ArrayList;

public class AVLTree {

	public class TreeNode {

		public String value;
		public TreeNode left;
		public TreeNode right;
		public TreeNode parent;
		public int balance;

		public TreeNode(String v) {
			left = right = parent = null;
			balance = 0;
			value = v;
		}
	}
	
    public int nodeVisitedCount;
    public int simpleRotationCount;
    public int doubleRotationCount;
	protected TreeNode root;
	
	public int currentSearchHeight = -1;

	public void insert(int value) {
		String values = String.format("%d",value);
		TreeNode n = new TreeNode(values);
		insertAvl(this.root, n);
	}

	private void insertAvl(TreeNode currentNode, TreeNode newNode) {
		
		if (currentNode == null) {
			this.root = newNode;
		} else {
			nodeVisitedCount++;
			if (newNode.value.compareTo(currentNode.value) < 0) {
				nodeVisitedCount++;
				if (currentNode.left == null) {
					currentNode.left = newNode;
					newNode.parent = currentNode;
					nodeVisitedCount++;
					recursiveBalance(currentNode);
				} else {
					nodeVisitedCount++;
					insertAvl(currentNode.left, newNode);
				}

			} else if (newNode.value.compareTo(currentNode.value) > 0) {
				nodeVisitedCount++;
				if (currentNode.right == null) {
					nodeVisitedCount++;
					currentNode.right = newNode;
					newNode.parent = currentNode;

					recursiveBalance(currentNode);
				} else {
					nodeVisitedCount++;
					insertAvl(currentNode.right, newNode);
				}
			} else {
				//System.out.println("\nValue " + newNode.value + " already exists. Ignoring...");
			}
		}
	}

	 public TreeNode find(String key) {
		
		currentSearchHeight = -1;
		return find(this.root, key);
	}

	 public TreeNode find(TreeNode currentNode, String value) {
		 currentSearchHeight++;
		 nodeVisitedCount++;
		 if (value.equals(currentNode.value))
			 return currentNode;
		 else if (currentNode.left != null || currentNode.right != null) {
			 nodeVisitedCount++;
			 if (currentNode.left != null) {
				 nodeVisitedCount++;
				 if (value.equals(currentNode.value)){
					 nodeVisitedCount++;
					 return currentNode;
				 }else if (value.compareTo(currentNode.value) < 0){
					 nodeVisitedCount++;
					 return find(currentNode.left, value);
				 }else if (currentNode.right != null){
					 nodeVisitedCount++;
					 return find(currentNode.right, value);
				 }else
					 return null;
			 }
			 if (value.equals(currentNode.value)){
				 nodeVisitedCount++;
				 return currentNode;
			 }else if (value.compareTo(currentNode.value) > 0){
				 nodeVisitedCount++;
				 return find(currentNode.right, value);
			 }else if (currentNode.left != null){
				 return find(currentNode.left, value);
			 }else
				 return null;
		 } else {
			 return null;
		 }
	 }

	 protected void recursiveBalance(TreeNode currentNode) {

		 setBalance(currentNode);
		 int balance = currentNode.balance;

		 if (balance == -2) {
			 if (height(currentNode.left.left) >= height(currentNode.left.right)) {
				 currentNode = rotateRight(currentNode);
			 } else {
				 currentNode = doubleRotateLeftRight(currentNode);
			 }
		 } else if (balance == 2) {
			 if (height(currentNode.right.right) >= height(currentNode.right.left)) {
				 currentNode = rotateLeft(currentNode);
			 } else {
				 currentNode = doubleRotateRightLeft(currentNode);
			 }
		 }

		 if (currentNode.parent != null) {
			 recursiveBalance(currentNode.parent);
		 } else {
			 this.root = currentNode;
		 }
	 }

	 public void remove(String k) {
		 currentSearchHeight = -1;
		 removeAvl(this.root, k);
	 }

	 private void removeAvl(TreeNode startingNode, String searchingKey) {
		 currentSearchHeight++;
		 nodeVisitedCount++;
		 if (startingNode == null) {
			 //System.out.println("\nKey " + searchingKey + " not found.");
		 } else {
			 if (startingNode.value.compareTo(searchingKey) > 0) {				 
				 removeAvl(startingNode.left, searchingKey);
			 } else if (startingNode.value.compareTo(searchingKey) < 0) {
				 removeAvl(startingNode.right, searchingKey);
			 } else if (startingNode.value.equals(searchingKey)) {
				 removeFoundNode(startingNode);
				 //System.out.println("\nValue " + searchingKey + " removed successfully at tree height " + currentSearchHeight + ".");
			 }
		 }
	 }

	 private void removeFoundNode(TreeNode removingNode) {
		 TreeNode parentNode;

		 if (removingNode.left == null || removingNode.right == null) {
			 parentNode = removingNode;
		 } else {
			 parentNode = successor(removingNode);
			 removingNode.value = parentNode.value;
		 }

		 TreeNode childNode;
		 if (parentNode.left != null) {
			 childNode = parentNode.left;
		 } else {
			 childNode = parentNode.right;
		 }

		 if (childNode != null) {
			 childNode.parent = parentNode.parent;
		 }

		 if (parentNode.parent == null) {
			 this.root = childNode;
		 } else {
			 if (parentNode == parentNode.parent.left) {
				 parentNode.parent.left = childNode;
			 } else {
				 parentNode.parent.right = childNode;
			 }
			 recursiveBalance(parentNode.parent);
		 }
		 parentNode = null;
	 }

	 private TreeNode rotateLeft(TreeNode rotatingNode) {
		 simpleRotationCount++;
		 TreeNode rotatedTreeRootNode = rotatingNode.right;
		 rotatedTreeRootNode.parent = rotatingNode.parent;

		 rotatingNode.right = rotatedTreeRootNode.left;

		 if (rotatingNode.right != null) {
			 rotatingNode.right.parent = rotatingNode;
		 }

		 rotatedTreeRootNode.left = rotatingNode;
		 rotatingNode.parent = rotatedTreeRootNode;

		 if (rotatedTreeRootNode.parent != null) {
			 if (rotatedTreeRootNode.parent.right == rotatingNode) {
				 rotatedTreeRootNode.parent.right = rotatedTreeRootNode;
			 } else if (rotatedTreeRootNode.parent.left == rotatingNode) {
				 rotatedTreeRootNode.parent.left = rotatedTreeRootNode;
			 }
		 }

		 setBalance(rotatingNode);
		 setBalance(rotatedTreeRootNode);

		 return rotatedTreeRootNode;
	 }

	 private TreeNode rotateRight(TreeNode rotatingNode) {
		 simpleRotationCount++;
		 TreeNode rotatedTreeRootNode = rotatingNode.left;
		 rotatedTreeRootNode.parent = rotatingNode.parent;

		 rotatingNode.left = rotatedTreeRootNode.right;

		 if (rotatingNode.left != null) {
			 rotatingNode.left.parent = rotatingNode;
		 }

		 rotatedTreeRootNode.right = rotatingNode;
		 rotatingNode.parent = rotatedTreeRootNode;

		 if (rotatedTreeRootNode.parent != null) {
			 if (rotatedTreeRootNode.parent.right == rotatingNode) {
				 rotatedTreeRootNode.parent.right = rotatedTreeRootNode;
			 } else if (rotatedTreeRootNode.parent.left == rotatingNode) {
				 rotatedTreeRootNode.parent.left = rotatedTreeRootNode;
			 }
		 }

		 setBalance(rotatingNode);
		 setBalance(rotatedTreeRootNode);

		 return rotatedTreeRootNode;
	 }

	 private TreeNode doubleRotateLeftRight(TreeNode rotatingNode) {
		 doubleRotationCount++;
		 rotatingNode.left = rotateLeft(rotatingNode.left);
		 return rotateRight(rotatingNode);
	 }

	 private TreeNode doubleRotateRightLeft(TreeNode rotatingNode) {
		 doubleRotationCount++;
		 rotatingNode.right = rotateRight(rotatingNode.right);
		 return rotateLeft(rotatingNode);
	 }

	 private TreeNode successor(TreeNode predecessorNode) {
		 TreeNode successorNode = null;

		 if (predecessorNode.left != null) {
			 successorNode = predecessorNode.left;
			 while (successorNode.right != null) {
				 successorNode = successorNode.right;
			 }
		 } else {
			 successorNode = predecessorNode.parent;
			 while (successorNode != null && predecessorNode == successorNode.left) {
				 predecessorNode = successorNode;
				 successorNode = predecessorNode.parent;
			 }
		 }

		 return successorNode;
	 }

	 public int height() {
		 return height(this.root);
	 }

	 private int height(TreeNode currentNode) {
		 if (currentNode == null) {
			 return -1;
		 }
		 if (currentNode.left == null && currentNode.right == null) {
			 return 0;
		 } else if (currentNode.left == null) {
			 return 1 + height(currentNode.right);
		 } else if (currentNode.right == null) {
			 return 1 + height(currentNode.left);
		 } else {
			 return 1 + maximum(height(currentNode.left), height(currentNode.right));
		 }
	 }

	 private int maximum(int a, int b) {
		 if (a >= b) {
			 return a;
		 } else {
			 return b;
		 }
	 }

	 private void getAll(TreeNode node) {
		 String l = "0";
		 String r = "0";
		 String p = "0";
		 if (node.left != null) {
			 l = node.left.value;
		 }
		 if (node.right != null) {
			 r = node.right.value;
		 }
		 if (node.parent != null) {
			 p = node.parent.value;
		 }

		// System.out.println("Left: " + l + " Value: " + node.value + " Right: " + r + " Parent: " + p + " Balance: " + node.balance);

		 if (node.left != null) {
			 getAll(node.left);
		 }
		 if (node.right != null) {
			 getAll(node.right);
		 }
	 }

	 private void setBalance(TreeNode currentNode) {
		 currentNode.balance = height(currentNode.right) - height(currentNode.left);
	 }

	 public TreeNode top() {
		 return this.root;
	 }

	 public TreeNode left() {
		 return this.root.left;
	 }

	 public TreeNode right() {
		 return this.root.right;
	 }

	 public ArrayList inOrder() {
		 ArrayList ret = new ArrayList();
		 inOrder(root, ret);
		 return ret;
	 }

	 private void inOrder(TreeNode node, ArrayList io) {
		 if (node == null) {
			 return;
		 }
		 inOrder(node.left, io);
		 io.add(node);
		 inOrder(node.right, io);
	 }

	 public int getCurrentSearchHeight() {
		return currentSearchHeight;
	}
	 
	 public int getDoubleRotationCount() {
		return doubleRotationCount;
	}
	 public int getNodeVisitedCount() {
		return nodeVisitedCount;
	}
	 public int getSimpleRotationCount() {
		return simpleRotationCount;
	}
	 
	 public void setDoubleRotationCount(int doubleRotationCount) {
		this.doubleRotationCount = doubleRotationCount;
	}
	 public void setSimpleRotationCount(int simpleRotationCount) {
		this.simpleRotationCount = simpleRotationCount;
	}
	 public void setNodeVisitedCount(int nodeVisitedCount) {
		this.nodeVisitedCount = nodeVisitedCount;
	}
	 
}