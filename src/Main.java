import java.util.Random;
import java.util.Scanner;

public class Main {
	private static final int INSERIR = 1;
	private static final int BUSCAR = 2;
	private static final int REMOVER = 3;
	
	public static void main(String[] args) {
		int option;
		RedBlackTree<Integer> blackTree = new RedBlackTree<>();
		AVLTree avl = new AVLTree();
		
		Scanner in = new Scanner(System.in);
		
		while(true){
			int valores[] = new int[10000];
			
			System.out.println("AVL x Rubro Negra");
			System.out.println("1 - Insira nos");
			System.out.println("2 - Buscar nos");
			System.out.println("3 - Remover nos");
			System.out.println("Outro - Sair");
			option = in.nextInt();
			Random gerador = new Random();
			switch (option) {			
			case INSERIR:
				blackTree.setDoubleRotationCount(0);
				blackTree.setSimpleRotationCount(0);
				blackTree.setNodeVisitedCount(0);
				avl.setDoubleRotationCount(0);
				avl.setSimpleRotationCount(0);
				avl.setNodeVisitedCount(0);
				for (int i = 0; i < 10000; i++){
					int valor =gerador.nextInt(1000000);
					blackTree.insert(valor);
					avl.insert(valor);
					valores[i]=valor;
				}	
				System.out.println("----------------INSER��O-------------");	
				System.out.println("---------------Rubro Negra-----------");
				System.out.println("Nos visitados: "+blackTree.getNodeVisitedCount());
				System.out.println("Numero de rotacoes simples:"+blackTree.getSimpleRotationCount());
				System.out.println("Numero de rotacoes duplas: "+blackTree.getDoubleRotationCount());
				System.out.println("------------------AVL----------------");
				System.out.println("Nos visitados: "+avl.getNodeVisitedCount());
				System.out.println("Numero de rotacoes simples:"+avl.getSimpleRotationCount());
				System.out.println("Numero de rotacoes duplas: "+avl.getDoubleRotationCount());
				System.out.println("------------------------------------");
				break;
			case BUSCAR:
				blackTree.setNodeVisitedCount(0);
				avl.setNodeVisitedCount(0);
				
				System.out.println("----------------BUSCA--------------");
				long mediaRb =0;
				long mediaAVL =0;
				
				for (int i = 0; i < 1000; i++) {
					blackTree.setNodeVisitedCount(0);	
					int v = valores[gerador.nextInt(10000)];
					blackTree.search(v);
					avl.find(String.format("%d",v));				
					mediaRb  += blackTree.getNodeVisitedCount();
					mediaAVL += avl.getNodeVisitedCount();
				}
				System.out.println("Media Rubro Negra: "+mediaRb/1000);
				System.out.println("Media Rubro Negra: "+mediaAVL/1000);
				System.out.println("------------------------------------");
				break;			
			case REMOVER:
				
				blackTree.setDoubleRotationCount(0);
				blackTree.setSimpleRotationCount(0);
				blackTree.setNodeVisitedCount(0);
				avl.setDoubleRotationCount(0);
				avl.setSimpleRotationCount(0);
				avl.setNodeVisitedCount(0);
				
				for (int i = 0; i < 1000; i++) {
					int root = blackTree.getRoot().key;
					blackTree.remove(root);
					avl.remove(String.format("%d",root));
				}
				System.out.println("----------------REMO��O--------------");
				System.out.println("---------------Rubro Negra-----------");
				System.out.println("Nos visitados: "+blackTree.getNodeVisitedCount());
				System.out.println("Numero de rotacoes simples:"+blackTree.getSimpleRotationCount());
				System.out.println("Numero de rotacoes duplas: "+blackTree.getDoubleRotationCount());
				System.out.println("------------------AVL----------------");
				System.out.println("Nos visitados: "+avl.getNodeVisitedCount());
				System.out.println("Numero de rotacoes simples:"+avl.getSimpleRotationCount());
				System.out.println("Numero de rotacoes duplas: "+avl.getDoubleRotationCount());
				System.out.println("------------------------------------");
				break;
			default:
				return;
			}			
		}
		
	}
}
